﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ganfer.API
{
    public class BD
    {
        public bool UpdateTarea(Tarea tarea)
        {
            using (GanferEntities context = new GanferEntities())
            {
                if (tarea.ID > 0) // UPDATE
                {
                    Tarea oldTarea = context.Tarea.Where(x => x.ID == tarea.ID).First();
                    if (tarea.Nombre != null)
                        oldTarea.Nombre = tarea.Nombre;
                    oldTarea.Completada = tarea.Completada;
                }
                else // CREATE
                    context.Tarea.Add(tarea);

                try
                {
                    context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public List<Tarea> GetTareas()
        {
            using (GanferEntities context = new GanferEntities())
            {
                return context.Tarea.ToList();
            }
        }
    }
}

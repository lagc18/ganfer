﻿$(document).ready(function () {
    var rows = [];
    $('#tblTarea').bootstrapTable({
        data: rows, formatNoMatches: function () {
            return 'Sin tareas registradas';
        },
    });

    $('#tareaModal').modal({
        backdrop: 'static', show: false
    })

    $('#tareaModal').on('shown.bs.modal', function (event) {
        $('#txtNombre').val('');
        $('#txtNombre').trigger('focus');
    });

    $('#tareas').submit(function (e) {
        e.preventDefault();

        var nombre = $('#txtNombre').val().trim();

        if (nombre == '')
            return alert('Ingrese un nombre');
        else {
            var tarea = new Object();
            tarea.Nombre = nombre;
            saveTarea(tarea);
        }
    });
});

function completadoFormat(value, row, index) {
    return value == 0 ? 'Incompleta' : 'Completa';
}

function getTareas(params) {
    $.ajax({
        type: 'POST',
        url: 'Default.aspx/GetTareas',
        data: '{ }',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json'
    }).done(function (response, textStatus, jqXHR) {
        let data = JSON.parse(response.d);
        params.success(data);
    });
}

function saveTarea(tarea) {
    $.ajax({
        type: 'POST',
        url: 'Default.aspx/UpdateTarea',
        data: '{ tarea: ' + JSON.stringify(tarea) + ' }',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json'
    }).done(function (response, textStatus, jqXHR) {
        let data = JSON.parse(response.d);
        if (data) {
            $('#tblTarea').bootstrapTable('refresh');
            $('#tareaModal').modal('hide');
        }
    });
}

window.operateEvents = {
    'click .check': function (e, value, row, index) {
        var tarea = new Object();
        tarea.ID = row.ID;
        tarea.Completada = true;
        saveTarea(tarea);
    },
}

function actionsFormatter(value, row, index) {
    if (row.Completada) {
        return [
            '<a class="text-muted">',
            '<i class="fa fa-check fa-lg"></i>',
            '</a>'
        ].join('')
    }
    else {
        return [
            '<a class="check text-success" href="javascript:void(0)" title="Completada">',
            '<i class="fa fa-check fa-lg"></i>',
            '</a>'
        ].join('')
    }
}
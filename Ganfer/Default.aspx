﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Ganfer.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Test - Ganfer</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table.min.js"></script>
    <script src="https://use.fontawesome.com/2b542b2305.js"></script>
    <script src="index.js"></script>
</head>
<body>
    <div class="container">
        <div class="card m-5">
            <div class="card-header">
                <h5 class="card-title">Tareas</h5>
            </div>
            <div class="card-body">
                <div class="d-flex justify-content-center">
                    <div class="table-responsive p-1">
                        <div class="py-2">
                            <button type="button" class="btn btn-primary accordion" data-toggle="modal" data-target="#tareaModal" data-whatever="Nueva">Nueva</button>
                        </div>
                        <table id="tblTarea" class="table-hover" data-ajax="getTareas">
                            <thead class="thead-dark">
                                <tr>
                                    <th data-field="ID" scope="col" data-visible="false">ID</th>
                                    <th data-field="Nombre" scope="col">Nombre</th>
                                    <th data-field="Completada" scope="col" data-formatter="completadoFormat">Completado</th>
                                    <th scope="col" data-formatter="actionsFormatter" data-events="window.operateEvents" data-width="100" data-align="center">Acciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="tareaModal" class="modal fade" tabindex="-1" aria-labelledby="tareaTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 id="tareaTitle" class="modal-title">Nueva Tarea</h5>
                </div>
                <div class="modal-body">
                    <form id="tareas">
                        <div class="col-12">
                            <div class="form-label-group">
                                <label for="txtNombre">Nombre</label>
                                <input type="text" class="form-control" id="txtNombre" maxlength="50" name="tareaName">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button type="submit" form="tareas" class="btn btn-success">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

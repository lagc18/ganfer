﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Newtonsoft.Json;
using Ganfer.API;

namespace Ganfer
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static bool UpdateTarea(Tarea tarea)
        {
            return new BD().UpdateTarea(tarea);
        }

        [WebMethod]
        public static string GetTareas()
        {
            return JsonConvert.SerializeObject(new BD().GetTareas());
        }
    }
}